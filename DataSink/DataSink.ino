// 8/2/22
// Import required libraries
#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include "ESPAsyncWebServer.h"
#include <WiFiClient.h>
#include <espnow.h>
#include <stdlib.h>

// Structure example to receive data
// Must match the sender structure
typedef struct Sensor_data {
  int id;
  double windSpeed;
  int windDir;
  float tempC;
  uint16_t capRead;
  float t;
  float h;
} my_Sensor_data;


// Create a struct_message called myData
my_Sensor_data myData;

// Create a struct_message called Data1
my_Sensor_data Data1; // Sensor node 1 id is 1
my_Sensor_data Data2; // Sensor node 2 id is 2
my_Sensor_data Data3; // Sensor node 3 id is 3

// Create an array with all the structures
my_Sensor_data sensorData[2] = {Data1, Data2};

int T; //period to send to node
unsigned long lastTime = 0;
unsigned long lastTime2 = 0;
unsigned long printTime = 0;
int Global_send_status = 1;
int countAlternate = 0;
bool newPeriod;
bool firstTime = true;

//-------------------------Node MCU Address-------------------------//
//NodeMCUs B,D,F are BROKEN
uint8_t broadcastAddressA[] = {0x4C, 0x75, 0x25, 0x35, 0x85, 0x80}; //NodeMCU A id is 1
uint8_t broadcastAddressC[] = {0x4C, 0x75, 0x25, 0x35, 0xB0, 0x11}; //NodeMCU C id is 3
uint8_t broadcastAddressE[] = {0x4C, 0x75, 0x25, 0x36, 0x57, 0x74}; //NodeMCU E id is 5
uint8_t broadcastAddressG[] = {0x58, 0xBF, 0x25, 0xDA, 0x84, 0x08}; //NodeMCU G id is 7
uint8_t broadcastAddressH[] = {0x40, 0x91, 0x51, 0x52, 0xD4, 0xB3}; //NodeMCU H id is 8
uint8_t broadcastAddressI[] = {0x58, 0xBF, 0x25, 0xDA, 0xA9, 0x19}; //NodeMCU I id is 9
uint8_t broadcastAddressJ[] = {0x58, 0xBF, 0x25, 0xD9, 0xE4, 0x13}; //NodeMCU J id is 10


//for the data send function
void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {
  char macStr[18];
  //  Serial.print("Packet to:");
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  //  Serial.print(macStr);
  //  Serial.print(" send status: ");

  Global_send_status = sendStatus;
  if (sendStatus == 0) {
    Serial.println("Period Delivery success");
    newPeriod = false;
    //        Global_send_status = 1;
  }
  else {
    Serial.println("Period Delivery fail");
  }
}




void storeData(int ID3) {
  Serial.println();
  Serial.print("ID: ");
  Serial.println(ID3);

  //Init EEPROM
  EEPROM.begin(4096);
  //Write data into eeprom

  int address = ID3 * 100; //spaces out the addresses enough not to interfere
  //Read data from eeprom
  int readId; //previous count
  EEPROM.get(address, readId);
  Serial.print("Old counter = ");
  Serial.println(readId);
  int counter = readId + 1; //incrementer

//    EEPROM.put(address, 0);  //used for setting counter back to 0
  EEPROM.put(address, counter); //regular counting
  EEPROM.commit();
  EEPROM.end();

  Serial.print("New Counter: ");
  Serial.println(counter); 
  Serial.println();
}


//for the Data Receive function
void OnDataRecv(uint8_t * mac_addr, uint8_t *incomingData, uint8_t len) {
  char macStr[18];
  //  Serial.print("Packet received from: ");
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  //  Serial.println(macStr);
  memcpy(&myData, incomingData, sizeof(myData));

  int array_Size = len / 40;
  //    Serial.print("size =  ");
  //    Serial.println(array_Size);

  // Update the structures with the new incoming data
  sensorData[myData.id - 1].id = myData.id;
  sensorData[myData.id - 1].windSpeed = myData.windSpeed;
  sensorData[myData.id - 1].windDir = myData.windDir;
  sensorData[myData.id - 1].tempC = myData.tempC;
  sensorData[myData.id - 1].capRead = myData.capRead;
  sensorData[myData.id - 1].t = myData.t;
  sensorData[myData.id - 1].h = myData.h;

  if (millis() - printTime > 100) {
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.print("Bytes received: ");
    Serial.println(len);
    Serial.print("Data Id: ");
    Serial.println(myData.id);
    Serial.print("Wind Speed: ");
    Serial.println(sensorData[myData.id - 1].windSpeed);
    Serial.print("Wind Direction: ");
    Serial.println(sensorData[myData.id - 1].windDir);
    Serial.print("Soil Temperature: ");
    Serial.println(sensorData[myData.id - 1].tempC);
    Serial.print("Soil Moisture: ");
    Serial.println(sensorData[myData.id - 1].capRead);
    Serial.print("Temperature: ");
    Serial.println(sensorData[myData.id - 1].t);
    Serial.print("Humidity: ");
    Serial.println(sensorData[myData.id - 1].h);

    storeData(myData.id); //stores counter for sensor node type
    printTime = millis();
  }
}

//-------------------------Web Server Start-------------------------//
//to store the Sensor ID input
const char* PARAM_INPUT_ID = "inputID";
const char* PARAM_INPUT_Period = "inputPeriod";
int id = 1;

// Replace with your network credentials
const char* ssid = "UCSC-Guest";
//const char* ssid = "csdudes";
//const char* password = "6.5nerds";

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);
//-------------------------Web Server End-------------------------//

String sendWindSpeed() {
  return String(sensorData[id - 1].windSpeed);
}
String sendWindDir() {
  return String(sensorData[id - 1].windDir);
}
String sendTempC() {
  return String(sensorData[id - 1].tempC);
}
String sendCapRead() {
  return String(sensorData[id - 1].capRead);
}
String sendTemp() {
  return String(sensorData[id - 1].t);
}
String sendHumidity() {
  return String(sensorData[id - 1].h);
}

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <style>
    html {
     font-family: Arial;
     display: inline-block;
     margin: 0px auto;
     text-align: center;
    }
    h2 { font-size: 3.0rem; }
    p { font-size: 3.0rem; }
    .units { font-size: 1.2rem; }
    .dht-labels{
      font-size: 1.5rem;
      vertical-align:middle;
      padding-bottom: 15px;
    }
  </style>
</head>
<body>
  <h2>SENSOR DATA</h2>
  <form action="/get">
    Sensor ID: <input type="number" name="inputID" min="1">
    <input type="submit" value="Submit">
  </form>
  <form action="/get">
    Sensor Period: <input type="number" name="inputPeriod" min="1">
    <input type="submit" value="Submit">
  </form>
  <p>
    <i class="fas fa-wind"></i> 
    <span class="dht-labels">Wind Speed</span> 
    <span id="windSpeed">%WINDSPEED%</span>
    <sup class="units">km/hr</sup>
  </p>
  <p>
    <i class="fas fa-compass"></i> 
    <span class="dht-labels">Wind Direction</span>
    <span id="windDirection">%WINDDIRECTION%</span>
    <sup class="units">&deg;</sup>
  </p>
  <p>
    <i class="fas fa-thermometer"></i> 
    <span class="dht-labels">Soil Temperature</span>
    <span id="soilTemp">%SOILTEMPERATURE%</span>
    <sup class="units">&deg;C</sup>
  </p>
  <p>
    <i class="fas fa-tint"></i> 
    <span class="dht-labels">Soil Moisture</span>
    <span id="soilHumidity">%SOILHUMIDITY%</span>
  </p>
  <p>
    <i class="fas fa-thermometer"></i> 
    <span class="dht-labels">Ambient Temperature</span>
    <span id="temperature">%TEMPERATURE%</span>
    <sup class="units">&deg;C</sup>
  </p>
  <p>
    <i class="fas fa-tint"></i> 
    <span class="dht-labels">Relative Ambient Humidity</span>
    <span id="humidity">%HUMIDITY%</span>
    <sup class="units">&percnt;</sup>
  </p>
</body>
<script>
//Wind Speed
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("windSpeed").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/windSpeed", true);
  xhttp.send();
}, 1000 ) ;

//wind Direction
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("windDirection").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/windDirection", true);
  xhttp.send();
}, 1000 ) ;

//Soil Temperature
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("soilTemp").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/soilTemp", true);
  xhttp.send();
}, 1000 ) ;

//Soil Humidity
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("soilHumidity").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/soilHumidity", true);
  xhttp.send();
}, 1000 ) ;

//Temperature
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("temperature").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/temperature", true);
  xhttp.send();
}, 1000 ) ;

//Humidity
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("humidity").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/humidity", true);
  xhttp.send();
}, 1000 ) ;
</script>
</html>)rawliteral";

// Replaces placeholder with DHT values
String processor(const String &var)
{
  // Serial.println(var);
  if (var == "WINDSPEED")
  {
    return sendWindSpeed();
  }
  else if (var == "WINDDIRECTION")
  {
    return sendWindDir();
  }
  else if (var == "SOILTEMPERATURE")
  {
    return sendTempC();
  }
  else if (var == "SOILHUMIDITY")
  {
    return sendCapRead();
  }
  else if (var == "TEMPERATURE")
  {
    return sendTemp();
  }
  else if (var == "HUMIDITY")
  {
    return sendHumidity();
  }
  return String();
}











void setup()
{
  // Serial port for debugging purposes
  Serial.begin(9600);

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_set_self_role(ESP_NOW_ROLE_COMBO);
  esp_now_register_send_cb(OnDataSent);
  //  esp_now_add_peer(broadcastAddressJ, ESP_NOW_ROLE_COMBO, 1, NULL, 0); //changing E to J
  esp_now_register_recv_cb(OnDataRecv);

  //    // Connect to Wi-Fi
  WiFi.begin(ssid);
  //    while (WiFi.status() != WL_CONNECTED)
  //    {
  //        delay(1000);
  //        Serial.println("Connecting to WiFi..");
  //    }
  //
  //    // Print ESP32 Local IP Address
  Serial.println(WiFi.localIP());



  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request)
  {
    request->send_P(200, "text/html", index_html, processor);
  });


  // Getting the ID input for the sensor needed to be displayed
  server.on("/get", HTTP_GET, [] (AsyncWebServerRequest * request) {
    String inputMessage;
    String inputParam;
    // GET ID value on <ESP_IP>/get?input1=<inputMessage>
    if (request->hasParam(PARAM_INPUT_ID)) {
      inputMessage = request->getParam(PARAM_INPUT_ID)->value();
      inputParam = PARAM_INPUT_ID;
      id = inputMessage.toInt();
    }
    else if (request->hasParam(PARAM_INPUT_Period)) {
      inputMessage = request->getParam(PARAM_INPUT_Period)->value();
      inputParam = PARAM_INPUT_Period;
      T = inputMessage.toInt();
      newPeriod = 1;
      Global_send_status == 1;
    } else {
      inputMessage = "No message sent";
      inputParam = "none";
    }
    //Serial.println(id);
    //Serial.println(T);
    request->send(200, "text/html", index_html);
  });

  // Sensors calling function
  server.on("/windSpeed", HTTP_GET, [](AsyncWebServerRequest * request)
  {
    request->send_P(200, "text/plain", sendWindSpeed().c_str());
  });
  server.on("/windDirection", HTTP_GET, [](AsyncWebServerRequest * request)
  {
    request->send_P(200, "text/plain", sendWindDir().c_str());
  });
  server.on("/soilTemp", HTTP_GET, [](AsyncWebServerRequest * request)
  {
    request->send_P(200, "text/plain", sendTempC().c_str());
  });
  server.on("/soilHumidity", HTTP_GET, [](AsyncWebServerRequest * request)
  {
    request->send_P(200, "text/plain", sendCapRead().c_str());
  });
  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest * request)
  {
    request->send_P(200, "text/plain", sendTemp().c_str());
  });
  server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest * request)
  {
    request->send_P(200, "text/plain", sendHumidity().c_str());
  });

  // Start server
  server.begin();

  //  newPeriod = true;
}




void loop() {
  if ( (millis() - lastTime > 6000) && (WiFi.status() == WL_CONNECTED) ) {
    //sends the period to the CH while WiFi is off

    //    Serial.println(newPeriod);
    //    Serial.println(Global_send_status == 1);
    if (newPeriod) {
      Serial.print("sending new period: ");
      Serial.println(T);
      esp_now_send(broadcastAddressJ, (uint8_t *) &T, sizeof(T));
    }
    WiFi.disconnect();
    lastTime = millis();
  } else if ((millis() - lastTime < 6000) && (WiFi.status() != WL_CONNECTED) ) {
    WiFi.begin(ssid);
    while (WiFi.status() != WL_CONNECTED)
    {
      delay(1000);
      //        Serial.println("Connecting to WiFi..");
    }
    if (firstTime) {
      Serial.print("IP for WebServer: ");
      Serial.println(WiFi.localIP());
      firstTime = false;
    }
  }
}
