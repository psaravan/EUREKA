// Team Eureka SDP 2021
// Sensor Node sensor measurement code

#include "Adafruit_seesaw.h"
#include "Adafruit_SHT31.h"
#define COMPASS_DIRECTIONS 16
#define WIND_SPEED_SENSOR  12

//anemomter variables
const int reading[COMPASS_DIRECTIONS] = {  78,  98, 108, 146, 207, 269, 315, 441, 498, 630, 672, 747, 825, 873, 932, 992};
const int compass[COMPASS_DIRECTIONS] = { 112,  67,  90, 157, 135, 202, 180,  22,  45, 247, 225, 337,   0, 292, 315, 270};
long windDirTot[COMPASS_DIRECTIONS];
unsigned int windSpeedCount = 0;
byte windSpeedEdgeCount = 0;
int prevWindSpeedSensor = LOW;
int period = 1; //period in seconds

//soil moisture variables
unsigned long DELAY_TIME = period * 1000;
unsigned long delayStart = 0; // the time the delay started
bool delayRunning = false; // true if still waiting for delay to finish
int xy = 0;
Adafruit_seesaw ss; //creates soil moisture sensor object

//temp/humidity variables
bool enableHeater = false;
uint8_t loopCnt = 0;
Adafruit_SHT31 sht31 = Adafruit_SHT31(); //creates temp sensor object

void setup() {
  Serial.begin(115200);
  delayStart = millis();
  delayRunning = true;
  //Serial.println("Count \tSpeed(KPH) \tDirection(degrees) \tHeading");
  pinMode(13, OUTPUT); //used to flash led corresponding to mux test
  digitalWrite(13, LOW);




  if (!ss.begin(0x36)) { //soil moisture sensor setup
    Serial.println("ERROR! seesaw not found");
    while (1) delay(1);
  } else {
    Serial.print("seesaw started! version: ");
    Serial.println(ss.getVersion(), HEX);
  }



  Serial.println("SHT31 test"); // temp/humid sensor setup
  if (! sht31.begin(0x44)) {   // Set to 0x45 for alternate i2c addr
    Serial.println("Couldn't find SHT31");
    while (1) delay(1);
  }
}


void loop() {
  // read the input on analog pin 0:
  double sensorValue = analogRead(A0);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 3.3V):
  // float voltage = sensorValue * (3.13 / 1023.0);
  double sensorValue2 = sensorValue * 0.95; //adjustement

  // Find most common wind direction over the reporting period
  long maxWindDirTot = 0;
  byte maxWindDirIndex = 0;
  for (byte i = 0; i < COMPASS_DIRECTIONS; i++) {
    if (windDirTot[i] > maxWindDirTot) {
      maxWindDirTot = windDirTot[i];
      maxWindDirIndex = i;
    }
    //Reset for next reporting period
    windDirTot[i] = 0;
  }
  int windDir = compass[maxWindDirIndex];
  byte i;
  for (i = 0; i < COMPASS_DIRECTIONS && sensorValue2 >= reading[i]; i++);
  windDirTot[i]++;

  int currWindSpeedSensor = digitalRead(WIND_SPEED_SENSOR);
  if (currWindSpeedSensor != prevWindSpeedSensor) {
    windSpeedEdgeCount++;
    if (windSpeedEdgeCount > 1) {
      windSpeedEdgeCount = 0;
      windSpeedCount++;
    }
    prevWindSpeedSensor = currWindSpeedSensor;
  }
  checkTimer(windDir); //calls function to average 3 seconds and print the current direction?
}

// Converts compass direction to heading
void getHeading(int direction) {
  if (direction < 22)
    Serial.println("N");
  else if (direction < 67)
    Serial.println("NE");
  else if (direction < 112)
    Serial.println("E");
  else if (direction < 157)
    Serial.println("SE");
  else if (direction < 212)
    Serial.println("S");
  else if (direction < 247)
    Serial.println("SW");
  else if (direction < 292)
    Serial.println("W");
  else if (direction < 337)
    Serial.println("NW");
  else
    Serial.println("N");
}

void checkTimer(int windDir) { // runs every period end
  // check if delay has timed out
  if (delayRunning && ((millis() - delayStart) >= DELAY_TIME)) {
    delayStart += DELAY_TIME; // this prevents drift in the delays

    // Calcualate average wind speed, in Km/hr, over reporting period
    // Note: 0.5 rotation per second = 2.4 Km/hr
    // 1 count per 0.5 rotation, so 1 count per millisecond = 2400 Km/hr
    double windSpeed =  2.4 * windSpeedCount / period;
    Serial.println("\n\t\nCount \tSpeed(KPH) \tDirection(degrees) \tHeading");
    Serial.print(windSpeedCount);
    Serial.print("\t");
    //Serial.print("\nWindSpeed: ");
    Serial.print(windSpeed);
    //Serial.print(" km/h");
    Serial.print("\t\t");
    //Serial.print("\nWind Direction: ");
    Serial.print(windDir);
    //Serial.print(" degrees");
    Serial.print("\t\t\t");
    getHeading(windDir);
    windSpeedCount = 0;
    xy += 1;

    if (xy % 2 == 1) { //alternates mux inputs
      digitalWrite(13, HIGH);
    } else {
      digitalWrite(13, LOW);
    }

    //soil moisture code
    float tempC = ss.getTemp();
    uint16_t capread = ss.touchRead(0);
    Serial.print("\nSoil Temp: "); Serial.print(tempC); Serial.print("*C");
    Serial.print("\t\tCapacitive Soil Moisture: "); Serial.println(capread);

    float t = sht31.readTemperature();
    float h = sht31.readHumidity();

    if (! isnan(t)) {  // check if 'is not a number'
      Serial.print("\nAmbient Temp: "); Serial.print(t); Serial.print("*C");
      //Serial.print("\nAmbient Temp *C = "); Serial.print(t); Serial.print("\t\t");
    } else {
      Serial.println("Failed to read temperature");
    }

    if (! isnan(h)) {  // check if 'is not a number'
      Serial.print("\t\tHum. % = "); Serial.println(h);
    } else {
      Serial.println("Failed to read humidity");
    }
  }
}
