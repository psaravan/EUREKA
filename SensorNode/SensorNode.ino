// 8/2/22
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <espnow.h>
#include "Adafruit_seesaw.h"
#include "Adafruit_SHT31.h"
#define COMPASS_DIRECTIONS 16

//gobal variables
int ID = 3;                 //ID for Sensor Node identification
int T;                 //duty cycle of entire sensor node, recieved from clusterhead
int T2;
int Global_send_status = 1; //boolean for success status

//timing variables
unsigned long delayStart = 0;                //the time the delay started
bool delayRunning = false;                   //true if still waiting for delay to finish
unsigned long lastTime = 0;                  //for the delay to send the timer

//anemometer variables
const int reading[COMPASS_DIRECTIONS] //map of 16 digital voltage values
  = {  78,  98, 108, 146, 207, 269, 315, 441, 498, 630, 672, 747, 825, 873, 932, 992};
const int compass[COMPASS_DIRECTIONS] //map of 16 anemometer angles
  = { 112,  67,  90, 157, 135, 202, 180,  22,  45, 247, 225, 337,   0, 292, 315, 270};
long windDirTot[COMPASS_DIRECTIONS];
unsigned int windSpeedCount = 0;
byte windSpeedEdgeCount = 0;
int prevWindSpeedSensor = LOW;

//temp/humidity variables
bool enableHeater = false;                    //heater for temp sensor
Adafruit_seesaw ss;                           //creates soil moisture sensor object
Adafruit_SHT31 sht31 = Adafruit_SHT31();      //creates temp sensor object

// Defining master mac address
//NodeMCUs B,D,F are BROKEN
uint8_t broadcastAddressA[] = {0x4C, 0x75, 0x25, 0x35, 0x85, 0x80}; //NodeMCU A id is 1
uint8_t broadcastAddressC[] = {0x4C, 0x75, 0x25, 0x35, 0xB0, 0x11}; //NodeMCU C id is 3
uint8_t broadcastAddressE[] = {0x4C, 0x75, 0x25, 0x36, 0x57, 0x74}; //NodeMCU E id is 5
uint8_t broadcastAddressG[] = {0x58, 0xBF, 0x25, 0xDA, 0x84, 0x08}; //NodeMCU G id is 7
uint8_t broadcastAddressH[] = {0x40, 0x91, 0x51, 0x52, 0xD4, 0xB3}; //NodeMCU H id is 8
uint8_t broadcastAddressI[] = {0x58, 0xBF, 0x25, 0xDA, 0xA9, 0x19}; //NodeMCU I id is 9
uint8_t broadcastAddressJ[] = {0x58, 0xBF, 0x25, 0xD9, 0xE4, 0x13}; //NodeMCU J id is 10



// Struct for sensor data, which matches that of the clusterhead
typedef struct Sensor_data {
  int id;
  double windSpeed;
  int windDir;
  float tempC;
  uint16_t capRead;
  float t;
  float h;
} Sensor_data;

// Create a struct for current data to be stored
Sensor_data Data1; // real data
//Sensor_data Data2; // testing fake data


// Callback when data is sent
void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {
  char macStr[18];
  //  Serial.print("Packet to:");
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  //  Serial.print(macStr);
  //  Serial.print(" send status: ");
  Global_send_status = sendStatus;
  if (sendStatus == 0) {
    Serial.println();
    //    Serial.println("Sensor Data Delivery success");
  }
  else {
    //    Serial.println("Sensor Data Delivery fail");
  }
}

// added receive callback function for receiving period from clusterhead
void OnDataRecv(uint8_t * mac, uint8_t * incomingperiod, uint8_t len) {
  memcpy(&T, incomingperiod, sizeof(T));  //saves this when recieved
  Serial.print("New period just received: "); Serial.println(T);  //prints for testing purposes
  storePeriod();
  getPeriod();
}






void setup() {
  //  for Sensor Data timing
  delayStart = millis();
  delayRunning = true;

  // Init Serial Monitor
  Serial.begin(9600);

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  //sets the role of mcu to two way comms
  esp_now_set_self_role(ESP_NOW_ROLE_COMBO);

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Transmitted packet
  esp_now_register_send_cb(OnDataSent);
  //  esp_now_add_peer(broadcastAddressC, ESP_NOW_ROLE_COMBO, 1, NULL, 0); //new
  esp_now_register_recv_cb(OnDataRecv); //new

  pinMode(13, OUTPUT); //GPIO output to flash led corresponding to mux test
  digitalWrite(13, LOW);
  pinMode(15, OUTPUT); //GPIO output to flash led corresponding to mux test
  digitalWrite(15, LOW);


  // For testing sending period
  //  if (Global_send_status == 1) {
  //    //Test for sending the period from the DS
  //    Serial.println("sending period");
  //    int T2 = 15;
  //    esp_now_send(broadcastAddressE, (uint8_t *) &T2, sizeof(T2));
  //  }

  getPeriod(); // sets the old period as the current period

  if (!ss.begin(0x36)) { //soil moisture sensor setup
    Serial.println("ERROR! seesaw not found");
//    while (1) delay(1); //prevents the loop from starting
  } else {
    Serial.print("seesaw started! version: ");
    Serial.println(ss.getVersion(), HEX);
  }

  Serial.println("SHT31 test"); // temp/humid sensor setup
  if (! sht31.begin(0x44)) {    // Set to 0x45 for alternate i2c addr
    Serial.println("Couldn't find SHT31");
    //while (1) delay(1); //prevents the loop from starting
  }

}







void loop() { //main loop
  if (millis() - lastTime) {
    // ID for Sensor Node
    Data1.id = ID;

    // read the input on analog pin 0:
    double sensorValue = analogRead(A0);
    // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 3.3V):
    double sensorValue2 = sensorValue * 0.95; //adjustement

    //  // Find most common wind direction over the reporting period
    long maxWindDirTot = 0;
    byte maxWindDirIndex = 0;
    for (byte i = 0; i < COMPASS_DIRECTIONS; i++) {
      if (windDirTot[i] > maxWindDirTot) {
        maxWindDirTot = windDirTot[i];
        maxWindDirIndex = i;
      }
      //Reset for next reporting period
      windDirTot[i] = 0;
    }
    Data1.windDir = compass[maxWindDirIndex];
    byte i;
    for (i = 0; i < COMPASS_DIRECTIONS && sensorValue2 >= reading[i]; i++);
    windDirTot[i]++;

    int currWindSpeedSensor = digitalRead(12);
    if (currWindSpeedSensor != prevWindSpeedSensor) {
      windSpeedEdgeCount++;
      if (windSpeedEdgeCount > 1) {
        windSpeedEdgeCount = 0;
        windSpeedCount++;
      }
      prevWindSpeedSensor = currWindSpeedSensor;
    }
    checkTimer(Data1.windDir); //calls function to average 3 seconds and print the current direction?

    //    delay(10); //delay to get rid of failures
    // Send message via ESP-NOW
    if (millis() >= 3100) { //measure for 3, then start sending
      delay (100);
      esp_now_send(broadcastAddressJ, (uint8_t *) &Data1, sizeof(Data1));
      // esp_now_send(broadcastAddressC, (uint8_t *) &Data2, sizeof(Data2)); //fake data test
      // Serial.println("sending new data");

      if ((millis() >= 30000)) { //awake untill 30 seconds passed
        Serial.println("Sensor Data Delivery Fail");
        sleep();
      }

      if ((Global_send_status == 0)) { //awake untill data recieved
        Serial.println("Sensor Data Delivery Success");
        sleep();
      }
    }

    lastTime = millis();
  }
}

void sleep() {
  int timer = (T * 1000) - millis();

  Serial.println();
  Serial.print("Current Period: ");
  Serial.println(T);
  if (T > 5) {
    Serial.println();
    Serial.print("I'm awake, but I'm going into deep sleep mode for seconds: ");
    //  Serial.println(timer);
    Serial.println(timer / 1000);
    if (timer > 0) {
      ESP.deepSleep(timer * 1000); //sleep for T - Y. Sleep argument is in nanoseconds
    } else {
      ESP.deepSleep(1 * 1000 * 1000); //sleeps for 1 second minimum (fire mode)
    }
  }
}


// Converts compass direction to heading
void getHeading(int direction) {
  if (direction < 22)
    Serial.println("N");
  else if (direction < 67)
    Serial.println("NE");
  else if (direction < 112)
    Serial.println("E");
  else if (direction < 157)
    Serial.println("SE");
  else if (direction < 212)
    Serial.println("S");
  else if (direction < 247)
    Serial.println("SW");
  else if (direction < 292)
    Serial.println("W");
  else if (direction < 337)
    Serial.println("NW");
  else
    Serial.println("N");
}



void sensorI2c() {
  //I2C Soil Moisture Sensor
  Data1.tempC = ss.getTemp() - 3; //-3C for adjustment accuracy
  Data1.capRead = ss.touchRead(0);
  Serial.print("\nSoil Temp: "); Serial.print(Data1.tempC); Serial.print("*C");
  Serial.print("\t\tCapacitive Soil Moisture: "); Serial.println(Data1.capRead);

  //I2C Ambient Temp/Humidity Sensor
  Data1.t = sht31.readTemperature();
  Data1.h = sht31.readHumidity();
  Serial.print("Ambient Temp: "); Serial.print(Data1.t); Serial.print("*C");
  Serial.print("\t\tAmbient Humidity: "); Serial.print(Data1.h); Serial.print("%");
}



void checkTimer(int windDir) { // runs every period end to average the wind speed
  // check if delay has timed out
  if (delayRunning && ((millis() - delayStart) >= 3000)) {
    delayStart += 3000; // this prevents drift in the delays

    // Calcualate average wind speed, in Km/hr, over reporting period
    // Note: 0.5 rotation per second = 2.4 Km/hr
    // 1 count per 0.5 rotation, so 1 count per millisecond = 2400 Km/hr
    Data1.windSpeed =  2.4 * windSpeedCount / 3;
    Serial.println("\n\t\nCount \tSpeed(KPH) \tDirection(degrees) \tHeading");
    Serial.print(windSpeedCount);
    Serial.print("\t");
    Serial.print(Data1.windSpeed);
    Serial.print("\t\t");
    Serial.print(windDir);
    Serial.print("\t\t\t");
    getHeading(windDir);
    windSpeedCount = 0;
    sensorI2c();             //calls function to collect soil measurements
  }
}


//stores new period
void storePeriod() {
  //Init EEPROM
  EEPROM.begin(128);

  Serial.print("Storing new period: ");
  Serial.println(T);
  //Write data into eeprom
  int address = 0;
  EEPROM.put(address, T);
  EEPROM.commit();
  EEPROM.end();
}


void getPeriod() {
  //Init EEPROM
  EEPROM.begin(128);

  //Reads current period
  int address = 0;
  EEPROM.get(address, T2);
  //  Serial.print("Read Id: ");
  //  Serial.println(T);
  //  address += sizeof(T); //update address value
  //  EEPROM.get(address, T); //readParam=EEPROM.readFloat(address);
  Serial.print(" Reading old period: ");
  Serial.println(T2);
  //printData(myData2);
  EEPROM.end();

  if (T2 == 0) {
    Serial.println("No old period read");
    T2 = 42;
  }
  T = T2;
}
