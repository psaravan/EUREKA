// 8/26/22
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <espnow.h>
#include "Adafruit_seesaw.h"
#include "Adafruit_SHT31.h"
#define COMPASS_DIRECTIONS 16

//global variables
int clustered = 1;
int Test;
int ID;                 //ID for Sensor Node identification
int T;                 //duty cycle of entire sensor node, recieved from clusterhead
int T2;
int Global_send_status = 1; //boolean for success status
int nodeDelay;

//timing variables
bool onced = true;
unsigned long asecond = 0;
unsigned long periodTimer = 0;
unsigned long periodStart = 0;
unsigned long delayStart = 0;                //the time the delay started
bool delayRunning = false;                   //true if still waiting for delay to finish
unsigned long lastTime = 0;                  //for the delay to send the timer

//anemometer variables
const int reading[COMPASS_DIRECTIONS] //map of 16 digital voltage values
  = {  78,  98, 108, 146, 207, 269, 315, 441, 498, 630, 672, 747, 825, 873, 932, 992};
const int compass[COMPASS_DIRECTIONS] //map of 16 anemometer angles
  = { 112,  67,  90, 157, 135, 202, 180,  22,  45, 247, 225, 337,   0, 292, 315, 270};
long windDirTot[COMPASS_DIRECTIONS];
unsigned int windSpeedCount = 0;
byte windSpeedEdgeCount = 0;
int prevWindSpeedSensor = LOW;

//temp/humidity variables
bool enableHeater = false;                    //heater for temp sensor
Adafruit_seesaw ss;                           //creates soil moisture sensor object
Adafruit_SHT31 sht31 = Adafruit_SHT31();      //creates temp sensor object

// Defining master mac address
uint8_t broadcastAddressALL[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}; //
uint8_t broadcastAddressCH[6];
uint8_t broadcastAddressCH2[6];


// Struct for sensor data, which matches that of the clusterhead
typedef struct Sensor_data {
  int id;
  double windSpeed;
  int windDir;
  float tempC;
  uint16_t capRead;
  float t;
  float h;
} Sensor_data;

// Create a struct for current data to be stored
Sensor_data Data1; // real data
//Sensor_data Data2; // testing fake data


// Callback when data is sent
void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {
  char macStr[18];
  Serial.print("Packet to:");
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  Serial.println(macStr);
  //  Serial.print(" send status: ");
  //  Serial.print(sendStatus);

  Global_send_status = sendStatus;
  if (sendStatus == 0) {
    Serial.println();
    //    Serial.println("Sensor Data Delivery success");
  }
  else {
    //    Serial.println("Sensor Data Delivery fail");
  }
}

// added receive callback function for receiving period from clusterhead
void OnDataRecv(uint8_t * mac_addr, uint8_t * incomingperiod, uint8_t len) {
  Serial.print("bytes: ");
  Serial.println(len);

  if (len == 4) {   //Broadcast received
    char macStr[18];
    Serial.print("Broadcast received from: ");
    snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
             mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
    Serial.println(macStr);
    broadcastAddressCH[0] = mac_addr[0], HEX;
    broadcastAddressCH[1] = mac_addr[1], HEX;
    broadcastAddressCH[2] = mac_addr[2], HEX;
    broadcastAddressCH[3] = mac_addr[3], HEX;
    broadcastAddressCH[4] = mac_addr[4], HEX;
    broadcastAddressCH[5] = mac_addr[5], HEX;
    storeMAC();
    getMAC();


    esp_now_send(broadcastAddressCH2, (uint8_t *) &Test, sizeof(Test));
    clustered = 1;
    Serial.println("Clustered");
    periodStart =  millis();//Starts ther period timer, which synchonizes the system together.

  } else { //schedule received
    uint32_t schedule[2];
    memcpy(&schedule, incomingperiod, sizeof(schedule));  //saves this when recieved
    ID = schedule[0];
    // ID for Sensor Node
    Data1.id = ID;
    T = schedule[1];
    Serial.print("ID: ");
    Serial.println(ID);
    Serial.print("New schedule just received: ");
    Serial.println(T);  //prints for testing purposes


    storePeriod();
    getPeriod();
    periodStart =  millis();//Starts ther period timer, which synchonizes the system together.
    //When periodStart = T then the node wakes up for the scheduling period.
    onced = true;
  }
}




void setup() {
  //  for Sensor Data timing
  delayStart = millis();
  delayRunning = true;
  // Init Serial Monitor
  Serial.begin(9600);

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  //sets the role of mcu to two way comms
  esp_now_set_self_role(ESP_NOW_ROLE_COMBO);
  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Transmitted packet
  esp_now_register_send_cb(OnDataSent);
  esp_now_register_recv_cb(OnDataRecv); //new

  getPeriod(); // sets the old period as the current period

  if (!ss.begin(0x36)) { //soil moisture sensor setup
    Serial.println("ERROR! seesaw not found");
    //    while (1) delay(1); //prevents the loop from starting
  } else {
    Serial.print("seesaw started! version: ");
    Serial.println(ss.getVersion(), HEX);
  }

  Serial.println("SHT31 test"); // temp/humid sensor setup
  if (! sht31.begin(0x44)) {    // Set to 0x45 for alternate i2c addr
    Serial.println("Couldn't find SHT31");
    //while (1) delay(1); //prevents the loop from starting
  }

  //delayed:
  //  while (clustered == 0) {
  //    goto delayed;
  //    delay(1000);
  //    Serial.println("delayed");
  //  }
  getMAC();
}







//RESET TIMER FOR 5 SECOND CLUSTERING
//STORE CLUSTERED STATE?
//MOST RECENT BROADCAST SHOULD ALWAYS PUT INTO SETUP PHASE
//STORE CLUSTER MAC?

void loop() { //main loop
  if ((millis() - asecond) > 1000) { //how often the broadcast is send out
    Serial.print("loop started, period timer: ");
    Serial.println(periodTimer / 1000);
    asecond = millis();
  }

  if (millis() - lastTime) {
    periodTimer = (millis() - periodStart); //calculates the time since schedule was received
    anemometer(); //averages data every 3 seconds



    nodeDelay = (5000 * (ID) + 100);
    if (clustered == 1) {
      if (onced && (periodTimer > nodeDelay)) { //wait for 5s, measure for 3s, then send ONCE
        Serial.println("");

        esp_now_send(broadcastAddressCH2, (uint8_t *) &Data1, sizeof(Data1));
        onced = false;
        delay(10);
        Serial.print("Wake in seconds: ");
        Serial.println((T * 1000 - periodTimer) / 1000);
        ESP.deepSleep((T * 1000 - periodTimer) * 1000);
      }
    } else {

    }
    lastTime = millis();
  }
}






void sleep() {
  // sleep(T-(Y-periodStart))

  int timer = (T * 1000) - periodTimer;
  Serial.println();
  Serial.print("Current Period: ");
  Serial.println(T);
  if (T > 5) {
    Serial.println();
    Serial.print("I'm awake, but I'm going into deep sleep mode for seconds: ");
    //  Serial.println(timer);
    Serial.println(timer / 1000);
    if (timer > 0) {
      //      ESP.deepSleep(timer * 1000); //sleep for T - Y. Sleep argument is in nanoseconds
    } else {
      //      ESP.deepSleep(1 * 1000 * 1000); //sleeps for 1 second minimum (fire mode)
    }
  }
}


// Converts compass direction to heading
void getHeading(int direction) {
  if (direction < 22)
    Serial.println("N");
  else if (direction < 67)
    Serial.println("NE");
  else if (direction < 112)
    Serial.println("E");
  else if (direction < 157)
    Serial.println("SE");
  else if (direction < 212)
    Serial.println("S");
  else if (direction < 247)
    Serial.println("SW");
  else if (direction < 292)
    Serial.println("W");
  else if (direction < 337)
    Serial.println("NW");
  else
    Serial.println("N");
}


void anemometer() {
  // read the input on analog pin 0:
  double sensorValue = analogRead(A0);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 3.3V):
  double sensorValue2 = sensorValue * 0.95; //adjustement

  //  // Find most common wind direction over the reporting period
  long maxWindDirTot = 0;
  byte maxWindDirIndex = 0;
  for (byte i = 0; i < COMPASS_DIRECTIONS; i++) {
    if (windDirTot[i] > maxWindDirTot) {
      maxWindDirTot = windDirTot[i];
      maxWindDirIndex = i;
    }
    //Reset for next reporting period
    windDirTot[i] = 0;
  }
  Data1.windDir = compass[maxWindDirIndex];
  byte i;
  for (i = 0; i < COMPASS_DIRECTIONS && sensorValue2 >= reading[i]; i++);
  windDirTot[i]++;

  int currWindSpeedSensor = digitalRead(12);
  if (currWindSpeedSensor != prevWindSpeedSensor) {
    windSpeedEdgeCount++;
    if (windSpeedEdgeCount > 1) {
      windSpeedEdgeCount = 0;
      windSpeedCount++;
    }
    prevWindSpeedSensor = currWindSpeedSensor;
  }
  checkTimer(Data1.windDir); //calls function to average 3 seconds and print the current direction?
}


void sensorI2c() {
  //I2C Soil Moisture Sensor
  Data1.tempC = ss.getTemp() - 3; //-3C for adjustment accuracy
  Data1.capRead = ss.touchRead(0);
  Serial.print("\nSoil Temp: "); Serial.print(Data1.tempC); Serial.print("*C");
  Serial.print("\t\tCapacitive Soil Moisture: "); Serial.println(Data1.capRead);

  //I2C Ambient Temp/Humidity Sensor
  Data1.t = sht31.readTemperature();
  Data1.h = sht31.readHumidity();
  Serial.print("Ambient Temp: "); Serial.print(Data1.t); Serial.print("*C");
  Serial.print("\t\tAmbient Humidity: "); Serial.print(Data1.h); Serial.print("%");
}



void checkTimer(int windDir) { // runs every period end to average the wind speed
  // check if delay has timed out
  if (delayRunning && ((millis() - delayStart) >= 12000)) { //????
    delayStart += 3000; // this prevents drift in the delays

    // Calcualate average wind speed, in Km/hr, over reporting period
    // Note: 0.5 rotation per second = 2.4 Km/hr
    // 1 count per 0.5 rotation, so 1 count per millisecond = 2400 Km/hr
    Data1.windSpeed =  2.4 * windSpeedCount / 3;
    Serial.println("\n\t\nCount \tSpeed(KPH) \tDirection(degrees) \tHeading");
    Serial.print(windSpeedCount);
    Serial.print("\t");
    Serial.print(Data1.windSpeed);
    Serial.print("\t\t");
    Serial.print(windDir);
    Serial.print("\t\t\t");
    getHeading(windDir);
    windSpeedCount = 0;
    sensorI2c();             //calls function to collect soil measurements
  }
}


//stores new period
void storePeriod() {
  //Init EEPROM
  EEPROM.begin(4096);

  Serial.print("Storing new period: ");
  Serial.println(T);
  //Write data into eeprom
  int address = 0;
  EEPROM.put(address, T);
  EEPROM.commit();
  EEPROM.end();
}


void getPeriod() {
  //Init EEPROM
  EEPROM.begin(4096);

  //Reads current period
  int address = 0;
  EEPROM.get(address, T2);
  //  Serial.print("Read Id: ");
  //  Serial.println(T);
  //  address += sizeof(T); //update address value
  //  EEPROM.get(address, T); //readParam=EEPROM.readFloat(address);
  Serial.print(" Reading old period: ");
  Serial.println(T2);
  //printData(myData2);
  EEPROM.end();

  if (T2 == 0) {
    Serial.println("No old period read");
    T2 = 42;
  }
  T = T2;
}




//stores new period
void storeMAC() {
  //Init EEPROM
  EEPROM.begin(4096);

  Serial.print("Storing CH MAC: ");
  Serial.println(broadcastAddressCH[5], HEX);
  //Write data into eeprom
  int address = 256;
  EEPROM.put(address, broadcastAddressCH);
  EEPROM.commit();
  EEPROM.end();
}


void getMAC() {
  //Init EEPROM
  EEPROM.begin(4096);

  //Reads current period
  int address = 256;
  EEPROM.get(address, broadcastAddressCH2);
  Serial.print(" Reading stored MAC: ");
  Serial.println(broadcastAddressCH2[5], HEX);
  //printData(myData2);
  EEPROM.end();

  //  if (broadcastAddressCH2 == 0) {
  //    Serial.println("No MAC read");
  //    broadcastAddressCH = broadcastAddressCH;
  //  }
  //  broadcastAddressCH = broadcastAddressCH2;
}
