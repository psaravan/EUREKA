/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com
*********/

// Import required libraries
#include <ESP8266WiFi.h>
#include "ESPAsyncWebServer.h"
#include <Adafruit_Sensor.h>
#include <SPI.h>
#include <WiFiClient.h>
#include <espnow.h>
#include <stdlib.h>

// Structure example to receive data
// Must match the sender structure
typedef struct Sensor_data {
  int id;
  double windSpeed;
  int windDir;
  float tempC;
  uint16_t capRead;
  float t;
  float h;
} my_Sensor_data;


// Create a struct_message called myData
my_Sensor_data myData;

// Create a struct_message called Data1
my_Sensor_data Data1; // Sensor node 1 id is 1
my_Sensor_data Data2; // Sensor node 2 id is 2
my_Sensor_data Data3; // Sensor node 3 id is 3

// Create an array with all the structures
my_Sensor_data sensorData[2] = {Data1, Data2};

int myperiod = 25; //period to send to node
unsigned long lastTime = 0;
unsigned long lastTime2 = 0;
unsigned long printTime = 0;
int Global_send_status = 1;
int countAlternate = 0;
bool newPeriod = true;

//-------------------------Node MCU Address-------------------------//
//NodeMCUs B,D,F are BROKEN
uint8_t broadcastAddressA[] = {0x4C, 0x75, 0x25, 0x35, 0x85, 0x80}; //NodeMCU A id is 1
uint8_t broadcastAddressC[] = {0x4C, 0x75, 0x25, 0x35, 0xB0, 0x11}; //NodeMCU C id is 3
uint8_t broadcastAddressE[] = {0x4C, 0x75, 0x25, 0x36, 0x57, 0x74}; //NodeMCU E id is 5
uint8_t broadcastAddressG[] = {0x58, 0xBF, 0x25, 0xDA, 0x84, 0x08}; //NodeMCU G id is 7
uint8_t broadcastAddressH[] = {0x40, 0x91, 0x51, 0x52, 0xD4, 0xB3}; //NodeMCU H id is 8
uint8_t broadcastAddressI[] = {0x58, 0xBF, 0x25, 0xDA, 0xA9, 0x19}; //NodeMCU I id is 9
uint8_t broadcastAddressJ[] = {0x58, 0xBF, 0x25, 0xD9, 0xE4, 0x13}; //NodeMCU J id is 10


//for the data send function
void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {
  char macStr[18];
  //  Serial.print("Packet to:");
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  //  Serial.print(macStr);
  //  Serial.print(" send status: ");
  Global_send_status = sendStatus;
  if (sendStatus == 0) {
    Serial.println("Period Delivery success");
    newPeriod = false;
  }
  else {
    //    Serial.println("Period Delivery fail");
  }
}

//for the Data Receive function
void OnDataRecv(uint8_t * mac_addr, uint8_t *incomingData, uint8_t len) {
  char macStr[18];
  //  Serial.print("Packet received from: ");
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  //  Serial.println(macStr);
  memcpy(&myData, incomingData, sizeof(myData));
  if (millis() - printTime > 1000) {


    // Update the structures with the new incoming data
    sensorData[myData.id - 1].id = myData.id;
    sensorData[myData.id - 1].windSpeed = myData.windSpeed;
    sensorData[myData.id - 1].windDir = myData.windDir;
    sensorData[myData.id - 1].tempC = myData.tempC;
    sensorData[myData.id - 1].capRead = myData.capRead;
    sensorData[myData.id - 1].t = myData.t;
    sensorData[myData.id - 1].h = myData.h;

    Serial.print("Bytes received: ");
    Serial.println(len);
    Serial.print("Data Id: ");
    Serial.println(sensorData[myData.id - 1].id);
    Serial.print("Wind Speed: ");
    Serial.println(sensorData[myData.id - 1].windSpeed);
    Serial.print("Wind Direction: ");
    Serial.println(sensorData[myData.id - 1].windDir);
    Serial.print("Soil Temperature: ");
    Serial.println(sensorData[myData.id - 1].tempC);
    Serial.print("Soil Moisture: ");
    Serial.println(sensorData[myData.id - 1].capRead);
    Serial.print("Temperature: ");
    Serial.println(sensorData[myData.id - 1].t);
    Serial.print("Humidity: ");
    Serial.println(sensorData[myData.id - 1].h);
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    printTime = millis();
  }
}

//-------------------------Web Server Start-------------------------//

//to store the Sensor ID input
const char* PARAM_INPUT_ID = "inputID";
const char* PARAM_INPUT_Period = "inputPeriod";
int id = 1;


// Replace with your network credentials
const char* ssid = "UCSC-Guest";
//const char* password = "6.5nerds";


// Create AsyncWebServer object on port 80
AsyncWebServer server(80);
//-------------------------Web Server End-------------------------//

String sendWindSpeed()
{
 
  Serial.println(sensorData[id-1].windSpeed);
  return String(sensorData[id-1].windSpeed);
}

String sendWindDir()
{
  Serial.println(sensorData[id-1].windDir);
  return String(sensorData[id-1].windDir);
}

String sendTempC()
{
  Serial.println(sensorData[id-1].tempC);
  return String(sensorData[id-1].tempC);
}

String sendCapRead()
{
  Serial.println(sensorData[id-1].capRead);
  return String(sensorData[id-1].capRead);
}

String sendTemp()
{
  Serial.println(sensorData[id-1].t);
  return String(sensorData[id-1].t);
}

String sendHumidity()
{
  Serial.println(sensorData[id-1].h);
  return String(sensorData[id-1].h);
}


const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <style>
    html {
     font-family: Arial;
     display: inline-block;
     margin: 0px auto;
     text-align: center;
    }
    h2 { font-size: 3.0rem; }
    p { font-size: 3.0rem; }
    .units { font-size: 1.2rem; }
    .dht-labels{
      font-size: 1.5rem;
      vertical-align:middle;
      padding-bottom: 15px;
    }
  </style>
</head>
<body>
  <h2>SENSOR DATA</h2>
  <form action="/get">
    Sensor ID: <input type="number" name="inputID" min="1">
    <input type="submit" value="Submit">
  </form>
  <form action="/get">
    Sensor Period: <input type="number" name="inputPeriod" min="1">
    <input type="submit" value="Submit">
  </form>
  <p>
    <i class="fas fa-wind"></i> 
    <span class="dht-labels">Wind Speed</span> 
    <span id="windSpeed">%WINDSPEED%</span>
    <sup class="units">m/s</sup>
  </p>
  <p>
    <i class="fas fa-compass"></i> 
    <span class="dht-labels">Wind Direction</span>
    <span id="windDirection">%WINDDIRECTION%</span>
    <sup class="units">&percnt;</sup>
  </p>
  <p>
    <i class="fas fa-thermometer"></i> 
    <span class="dht-labels">Soil Temperature</span>
    <span id="soilTemp">%SOILTEMPERATURE%</span>
    <sup class="units">&deg;C</sup>
  </p>
  <p>
    <i class="fas fa-tint"></i> 
    <span class="dht-labels">Soil Humidity</span>
    <span id="soilHumidity">%SOILHUMIDITY%</span>
    <sup class="units">&percnt;</sup>
  </p>
  <p>
    <i class="fas fa-thermometer"></i> 
    <span class="dht-labels">Temperature</span>
    <span id="temperature">%TEMPERATURE%</span>
    <sup class="units">&deg;C</sup>
  </p>
  <p>
    <i class="fas fa-tint"></i> 
    <span class="dht-labels">Humidity</span>
    <span id="humidity">%HUMIDITY%</span>
    <sup class="units">&percnt;</sup>
  </p>
</body>
<script>
//Wind Speed
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("windSpeed").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/windSpeed", true);
  xhttp.send();
}, 1000 ) ;

//wind Direction
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("windDirection").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/windDirection", true);
  xhttp.send();
}, 1000 ) ;

//Soil Temperature
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("soilTemp").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/soilTemp", true);
  xhttp.send();
}, 1000 ) ;

//Soil Humidity
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("soilHumidity").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/soilHumidity", true);
  xhttp.send();
}, 1000 ) ;

//Temperature
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("temperature").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/temperature", true);
  xhttp.send();
}, 1000 ) ;

//Humidity
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("humidity").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/humidity", true);
  xhttp.send();
}, 1000 ) ;
</script>
</html>)rawliteral";

// Replaces placeholder with DHT values
String processor(const String &var)
{
    // Serial.println(var);
    if (var == "WINDSPEED")
    {
        return sendWindSpeed();
    }
    else if (var == "WINDDIRECTION")
    {
        return sendWindDir();
    }
    else if (var == "SOILTEMPERATURE")
    {
        return sendTempC();
    }
    else if (var == "SOILHUMIDITY")
    {
        return sendCapRead();
    }
    else if (var == "TEMPERATURE")
    {
        return sendTemp();
    }
    else if (var == "HUMIDITY")
    {
        return sendHumidity();
    }
    return String();
}

void setup()
{
    // Serial port for debugging purposes
    Serial.begin(115200);

    // Set device as a Wi-Fi Station
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();

  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_set_self_role(ESP_NOW_ROLE_COMBO);
  esp_now_register_send_cb(OnDataSent);
  esp_now_add_peer(broadcastAddressJ, ESP_NOW_ROLE_COMBO, 1, NULL, 0); //changing E to J
  esp_now_add_peer(broadcastAddressG, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
  esp_now_register_recv_cb(OnDataRecv);
    
    // Connect to Wi-Fi
    WiFi.begin(ssid);
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(1000);
        Serial.println("Connecting to WiFi..");
    }

    // Print ESP32 Local IP Address
    Serial.println(WiFi.localIP());

    // Testing Purpose
// Update the structures with the new incoming data
sensorData[0].id = 1;
sensorData[0].windSpeed = 200;
sensorData[0].windDir = 200;
sensorData[0].tempC = 200;
sensorData[0].capRead = 200;
sensorData[0].t = 200;
sensorData[0].h = 200;

// Update the structures with the new incoming data
sensorData[1].id = 2;
sensorData[1].windSpeed = 100;
sensorData[1].windDir = 100;
sensorData[1].tempC = 100;
sensorData[1].capRead = 100;
sensorData[1].t = 100;
sensorData[1].h = 100;

    // Route for root / web page
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send_P(200, "text/html", index_html, processor); });

     
    // Getting the ID input for the sensor needed to be displayed
    server.on("/get", HTTP_GET, [] (AsyncWebServerRequest *request) {
      String inputMessage;
      String inputParam;
      // GET ID value on <ESP_IP>/get?input1=<inputMessage>
      if (request->hasParam(PARAM_INPUT_ID)) {
        inputMessage = request->getParam(PARAM_INPUT_ID)->value();
        inputParam = PARAM_INPUT_ID;
        id = inputMessage.toInt();
      }
      else if(request->hasParam(PARAM_INPUT_Period)){
        inputMessage = request->getParam(PARAM_INPUT_Period)->value();
        inputParam = PARAM_INPUT_Period;
        myperiod = inputMessage.toInt();
        newPeriod = true;
      }else{
        inputMessage = "No message sent";
        inputParam = "none";
      }
      Serial.println(id);
      Serial.println(myperiod);
      request->send(200, "text/html",index_html);
    });

    // Sensors calling function
    server.on("/windSpeed", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send_P(200, "text/plain", sendWindSpeed().c_str()); });
    server.on("/windDirection", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send_P(200, "text/plain", sendWindDir().c_str()); });
    server.on("/soilTemp", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send_P(200, "text/plain", sendTempC().c_str()); });
    server.on("/soilHumidity", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send_P(200, "text/plain", sendCapRead().c_str()); });
    server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send_P(200, "text/plain", sendTemp().c_str()); });
    server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send_P(200, "text/plain", sendHumidity().c_str()); });

    // Start server
    server.begin();
}

void loop() {
  // After 10 second intervals change the period, if recieved successfully
  if (((millis() - lastTime) > 40000) && Global_send_status == 0 && newPeriod == 0) {
//    newPeriod = true;

//    countAlternate = countAlternate + 1;
//    if (countAlternate % 2 == 0) {
//      myperiod = 20;
//    }
//    else {
//      myperiod = 10;
//    }
    Serial.print("Current Period:");
    Serial.println(myperiod);
    Serial.println();
    //    Serial.println(countAlternate);
    //    Serial.println();
    lastTime = millis();
  }
  if ((millis() - lastTime2 > 1000)) { //print period status every second
    Serial.println(newPeriod);
    lastTime2 = millis();

  }
  // When the period is not recieved correctly yet, keep sending the period
  if (Global_send_status == 1 || newPeriod) {
    delay(10);
    Serial.println("sending new period: ");
    Serial.println(myperiod);
    esp_now_send(broadcastAddressJ, (uint8_t *) &myperiod, sizeof(myperiod));    //for sending myperiod to node.  Not sure if the formatting is correct)
    //    esp_now_send(broadcastAddressG, (uint8_t *) &myperiod, sizeof(myperiod));    //for sending myperiod to node.  Not sure if the formatting is correct)
  }

  else
  {
    //    if ((millis() >= 120000)) { //awake untill data recieved
    //          Serial.println("I'm awake, but I'm going into deep sleep mode for seconds:");
    //          Serial.println(60);
    //          Serial.println(((60 * 1000) - millis()));
    //          ESP.deepSleep(((60 * 1000) - millis()) * 1000); //sleep for T - Y
    //     }
  }
}
