// 8/2/22
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <espnow.h>

// Structure example to receive data
// Must match the sender structure
typedef struct Sensor_data {
  int id;
  double windSpeed;
  int windDir;
  float tempC;
  uint16_t capRead;
  float t;
  float h;
} my_Sensor_data; //delete later


// Create a struct_message called myData
my_Sensor_data myData;

// Create a struct_message called Data1
my_Sensor_data Data1; // Sensor node 1 id is 1
my_Sensor_data Data2; // Sensor node 2 id is 2
my_Sensor_data Data3; // Sensor node 3 id is 3
my_Sensor_data sendDatafinal;


// Create an array with all the structures
my_Sensor_data sensorData[3] = {Data1, Data2, Data3};
//my_Sensor_data allNodes[3] = {node1, node2, node3};


int T;  //previous period to send to node
unsigned long lastTime = 0;
unsigned long lastTime2 = 0;
unsigned long printTime = 0;
int Data_send_status = 0;
int Period1_send_status = 0;
int Period2_send_status = 0;
int Period3_send_status = 0;


int countAlternate = 0;
bool newPeriod;
bool dataRec0 = false;
bool dataRec1 = false;
bool dataRec2 = false;


//NodeMCUs B,D,F are BROKEN
uint8_t broadcastAddressA[] = {0x4C, 0x75, 0x25, 0x35, 0x85, 0x80}; //NodeMCU A id is 1
uint8_t broadcastAddressC[] = {0x4C, 0x75, 0x25, 0x35, 0xB0, 0x11}; //NodeMCU C id is 3
uint8_t broadcastAddressE[] = {0x4C, 0x75, 0x25, 0x36, 0x57, 0x74}; //NodeMCU E id is 5
uint8_t broadcastAddressG[] = {0x58, 0xBF, 0x25, 0xDA, 0x84, 0x08}; //NodeMCU G id is 7
uint8_t broadcastAddressH[] = {0x40, 0x91, 0x51, 0x52, 0xD4, 0xB3}; //NodeMCU H id is 8
uint8_t broadcastAddressI[] = {0x58, 0xBF, 0x25, 0xDA, 0xA9, 0x19}; //NodeMCU I id is 9
uint8_t broadcastAddressJ[] = {0x58, 0xBF, 0x25, 0xD9, 0xE4, 0x13}; //NodeMCU J id is 10


void getHeading(int direction) {
  if (direction < 22)
    Serial.println("N");
  else if (direction < 67)
    Serial.println("NE");
  else if (direction < 112)
    Serial.println("E");
  else if (direction < 157)
    Serial.println("SE");
  else if (direction < 212)
    Serial.println("S");
  else if (direction < 247)
    Serial.println("SW");
  else if (direction < 292)
    Serial.println("W");
  else if (direction < 337)
    Serial.println("NW");
  else
    Serial.println("N");
}

void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {
  char macStr[18];
  //  Serial.print("Packet to:");
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  //    Serial.println(macStr);
  //    Serial.print(" send status: ");

  //this below maps the send status based on outgoing MACadr
  //  Serial.println(mac_addr[0]);
  //  Serial.println(broadcastAddressC[0]);

  if ((mac_addr[5] == broadcastAddressI[5])) {   //sending data to clusterhead
    Data_send_status = sendStatus;
  }
  if ((mac_addr[5] == broadcastAddressC[5]) && (sendStatus == 0)) {   //sending period to node1. only sets the status if its a success
    Period1_send_status = sendStatus;
  }
  if ((mac_addr[5] == broadcastAddressE[5]) && (sendStatus == 0)) {   //sending period to node2
    Period2_send_status = sendStatus;
  }
  if ((mac_addr[5] == broadcastAddressA[5]) && (sendStatus == 0)) {   //sending period to node3
    Period3_send_status = sendStatus;
  }

  //    Used to test the if statement below
  //    Serial.print("newPeriod: ");
  //    Serial.println(newPeriod);
  //    Serial.print("Period send status: ");
  //    Serial.print(Period1_send_status);
  //    Serial.print(Period2_send_status);
  //    Serial.println(Period3_send_status);

  if (!Period1_send_status && !Period2_send_status && !Period3_send_status && newPeriod) {
    Serial.println("All Period Delivery success");
    newPeriod = false;
    // for all nodes
  }
  else {
    //    Serial.println("Period Delivery fail");
  }
}





void OnDataRecv(uint8_t * mac_addr, uint8_t *incomingThing, uint8_t len) {
  char macStr[18];
  //Serial.print("Packet received from: ");
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  //Serial.println(macStr);

  if (len <= 20) {   //if receiving period
    Serial.print("Period bytes: ");
    Serial.println(len);
    newPeriod = true; //indicates that a new period was received and needs to be sent to nodes
    //    Serial.println(newPeriod);
    Period1_send_status = 1;
    Period2_send_status = 1;
    Period3_send_status = 1;

    //    Serial.print("New period just received: "); Serial.println(T);  //prints for testing purposes
    memcpy(&T, incomingThing, sizeof(T));  //saves this when recieved
    Serial.print("New period just received: "); Serial.println(T);  //prints for testing purposes

    //    esp_now_send(broadcastAddressG, (uint8_t *) &T, sizeof(T));    //for sending T to node.  Not sure if the formatting is correct)

  } else {  //if receiving sensor data
    Data_send_status = 1;
    memcpy(&myData, incomingThing, sizeof(myData));
    //    Serial.println(myData.id);
  }

  if (millis() - printTime > 100) { //stores and prints the data every 0.1 second
    //    Serial.println();
    //    Serial.print("bytes: ");
    //    Serial.println(len);
    // Update the structures with the new incoming data
    sensorData[myData.id - 1].id = myData.id;
    sensorData[myData.id - 1].windSpeed = myData.windSpeed;
    sensorData[myData.id - 1].windDir = myData.windDir;
    sensorData[myData.id - 1].tempC = myData.tempC;
    sensorData[myData.id - 1].capRead = myData.capRead;
    sensorData[myData.id - 1].t = myData.t;
    sensorData[myData.id - 1].h = myData.h;

    printData();
    printTime = millis();
  }
}






void printData() { //prints all data
  Serial.println("\n");
  Serial.print("Data Id: ");
  Serial.println(myData.id);

  Serial.println("WindSpeed(KPH) \tDirection(degrees) \tHeading");
  Serial.print(myData.windSpeed);
  Serial.print("\t\t");
  Serial.print(myData.windDir);
  Serial.print("\t\t\t");
  getHeading(myData.windDir);

  Serial.print("Soil Temp: "); Serial.print(myData.tempC); Serial.print("*C");
  Serial.print("\t\tCapacitive Soil Moisture: "); Serial.println(myData.capRead);
  Serial.print("Ambient Temp: "); Serial.print(myData.t); Serial.print("*C");
  Serial.print("\t\tAmbient Humidity: "); Serial.println(myData.h);
}





void setup() {
  //  T = 308; newPeriod = true; //For manually setting the period

  // Initialize Serial Monitor
  Serial.begin(9600);
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_set_self_role(ESP_NOW_ROLE_COMBO);
  esp_now_register_send_cb(OnDataSent);
  // esp_now_add_peer(broadcastAddressI, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
  // esp_now_add_peer(broadcastAddressE, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
  // esp_now_add_peer(broadcastAddressC, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
  esp_now_register_recv_cb(OnDataRecv);
}





void loop() {
  if (millis() - lastTime > 100) {

    int array_Size = sizeof(sensorData) / 40;
    //      Serial.print("size =  ");
    //      Serial.println(array_Size);
    //
    //int i = 0;
    //        sensorData[i].id = 1;
    //        sensorData[i].windSpeed = 9 + sensorData[i].windSpeed;
    //        sensorData[i].windDir = 20 + sensorData[i].windDir;
    //        sensorData[i].tempC = 30 + sensorData[i].tempC ;
    //        sensorData[i].capRead = 40 + sensorData[i].capRead;
    //        sensorData[i].t = 51 + sensorData[i].t;
    //        sensorData[i].h = 60 + sensorData[i].h;
    //
    //        i = 1;
    //        sensorData[i].id = 2;
    //        sensorData[i].windSpeed = 9 + sensorData[i].windSpeed;
    //        sensorData[i].windDir = 20 + sensorData[i].windDir;
    //        sensorData[i].tempC = 30 + sensorData[i].tempC ;
    //        sensorData[i].capRead = 40 + sensorData[i].capRead;
    //        sensorData[i].t = 51 + sensorData[i].t;
    //        sensorData[i].h = 60 + sensorData[i].h;
    //
    //    i = 2;
    //    sensorData[i].id = 3;
    //    sensorData[i].windSpeed = 9 + sensorData[i].windSpeed;
    //    sensorData[i].windDir = 20 + sensorData[i].windDir;
    //    sensorData[i].tempC = 30 + sensorData[i].tempC ;
    //    sensorData[i].capRead = 40 + sensorData[i].capRead;
    //    sensorData[i].t = 51 + sensorData[i].t;
    //    sensorData[i].h = 60 + sensorData[i].h;


    for (int i = 1; i < array_Size; i++) {
      sendDatafinal.id = sensorData[i].id;
      sendDatafinal.windSpeed = sensorData[i].windSpeed;
      sendDatafinal.windDir = sensorData[i].windDir;
      sendDatafinal.tempC = sensorData[i].tempC;
      sendDatafinal.capRead = sensorData[i].capRead;
      sendDatafinal.t = sensorData[i].t;
      sendDatafinal.h = sensorData[i].h;
      //      while (Data_send_status == 1) {
      //        Serial.println(sendDatafinal.id);
      //        esp_now_send(broadcastAddressI, (uint8_t *) &sendDatafinal, sizeof(sendDatafinal));
      //        //      esp_now_send(broadcastAddressI, (uint8_t *) &sensorData, sizeof(sensorData));
      //      }
    }

    if (Data_send_status == 1) {
      //      Serial.print("Sending data to DS, ID: "); Serial.println(myData.id);
      esp_now_send(broadcastAddressI, (uint8_t *) &myData, sizeof(myData));
    }
    lastTime = millis();
  }


  if ((millis() - lastTime2 > 2000)) { //print period status every second
    //    Serial.print("newPeriod: ");
    //    Serial.println(newPeriod);
    //    Serial.print("Period send status: ");
    //    Serial.print(Period1_send_status);
    //    Serial.println(Period2_send_status);

    lastTime2 = millis();
  }


  if (newPeriod) { //send if there is a new period
    delay(100); //prevents crashing

    //    Serial.print("sending new period: ");
    //    Serial.println(T);

    esp_now_send(broadcastAddressC, (uint8_t *) &T, sizeof(T));    //for sending T to node.  Not sure if the formatting is correct)
    esp_now_send(broadcastAddressE, (uint8_t *) &T, sizeof(T));    //for sending T to node.  Not sure if the formatting is correct)
    esp_now_send(broadcastAddressA, (uint8_t *) &T, sizeof(T));    //for sending T to node.  Not sure if the formatting is correct)
  }

  //  if ((millis() >= 30000)) { //awake untill 30s seconds passed (timeout)
  //    Serial.println("Period Delivery Fail");
  //    storeData();
  //    sleep();
  //  }
  //    if (dataRec0 && dataRec1 && dataRec2 && Global_send_status == 0) {
  //      //awake untill all data recieved, and periods sent successfully
  //      Serial.println("All Delivery Success");
  //      sleep();
  //    }
}












//sends the CH to sleep for the rest of the period
void sleep() {
  Serial.println();
  Serial.print("Current Period: ");
  Serial.println(T);
  Serial.println();
  Serial.print("I'm awake, but I'm going into deep sleep mode for seconds : ");
  //  Serial.println((T * 1000) - millis());
  Serial.println(((T * 1000) - millis()) / 1000);
  ESP.deepSleep(((T * 1000) - millis()) * 1000); //sleep for T - Y. Sleep argument is in nanoseconds
}





//stores data that wasn't sent successfully. Stores all 3 nodes data.
void storeData() {
  //Init EEPROM
  EEPROM.begin(4096);
  //Write data into eeprom
  int address = 0;
  EEPROM.put(address, sensorData);
  EEPROM.commit();

  //Read data from eeprom
  address = 0;
  int readId;
  EEPROM.get(address, readId);
  //Serial.print("Read Id = ");
  //Serial.println(readId);
  address += sizeof(readId); //update address value
  EEPROM.get(address, sensorData); //readParam=EEPROM.readFloat(address);
  //printData(myData2);
  EEPROM.end();
}
