// 8/26/22
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <espnow.h>

// Structure example to receive data
// Must match the sender structure
typedef struct Sensor_data {
  int id;
  double windSpeed;
  int windDir;
  float tempC;
  uint16_t capRead;
  float t;
  float h;
} my_Sensor_data; //delete later


// Create a struct_message called myData
my_Sensor_data myData;

// Create a struct_message called Data1
my_Sensor_data Data1; // Sensor node 1 id is 1
my_Sensor_data Data2; // Sensor node 2 id is 2
my_Sensor_data Data3; // Sensor node 3 id is 3
my_Sensor_data sendDatafinal;


// Create an array with all the structures
my_Sensor_data sensorData[3] = {Data1, Data2, Data3};
//my_Sensor_data allNodes[3] = {node1, node2, node3};

int MacNum = 0; // amount of unique mac addresses
int Test;
int firstCluster; //set to 1 if this is the first cycle
int alwaysCluster = 1; //set to 1 to force cluster every period
int T = 25;  //previous period to send to node
int x = 0;

int Data_send_status = 0;
int Period1_send_status = 0;
int Period2_send_status = 0;
int Period3_send_status = 0;
int countAlternate = 0;

unsigned long hundred = 0;
unsigned long ahsecond = 0;
unsigned long asecond = 0;
unsigned long periodStart = 0;
unsigned long periodTimer = 0;
unsigned long lastTime = 0;
unsigned long lastTime2 = 0;
unsigned long lastTime3 = 0;
unsigned long lastTime4 = 0;
unsigned long printTime = 0;

bool scheduling = true;
bool Cluster = true;
bool clustered = false;
bool newPeriod;
bool dataRec0 = false;
bool dataRec1 = false;
bool dataRec2 = false;
const int nodes = 100; // max number of nodes the system can handle

uint8_t broadcastAddressDS[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; //
uint8_t broadcastAddressALL[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}; //
uint8_t MacList[nodes][6];

void getHeading(int direction) {
  if (direction < 22)
    Serial.println("N");
  else if (direction < 67)
    Serial.println("NE");
  else if (direction < 112)
    Serial.println("E");
  else if (direction < 157)
    Serial.println("SE");
  else if (direction < 212)
    Serial.println("S");
  else if (direction < 247)
    Serial.println("SW");
  else if (direction < 292)
    Serial.println("W");
  else if (direction < 337)
    Serial.println("NW");
  else
    Serial.println("N");
}

void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {
  char macStr[18];
  Serial.print("Packet to:");
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  Serial.println(macStr);
  //  Serial.print(" send status: ");
  //  Serial.println(sendStatus);
}




void OnDataRecv(uint8_t * mac_addr, uint8_t *incomingThing, uint8_t len) {
  char macStr[18];
  Serial.println(" ");
  //  Serial.print("bits: ");
  //  Serial.println(len);

  /////////////////////////////////////////////////
  if (len < 8) {
    Serial.print("Reply received from: ");
    snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
             mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
    Serial.println(macStr);

    int dupe = 0;
    //  Serial.println("comparing to all macs");

    for ( int i = 0; i < MacNum; i++ ) {
      int same = 0;
      //    Serial.print("comparing to mac: ");
      //    Serial.println(i + 1);
      for ( int j = 0; j < 6; j++ ) {
        //      Serial.print(MacList[i][j]), HEX;
        //      Serial.println(mac_addr[j]), HEX;
        //      Serial.print("MacList[4][j]: ");
        //      Serial.println(MacList[4][j]), HEX;
        if (MacList[i][j] == mac_addr[j]) {
          same++;
          //        Serial.print("total same bits: ");
          //        Serial.println(same);
        }
      }
      if (same == 6) {
        dupe++;
        Serial.print("same mac as mac: ");
        Serial.println(i + 1);
        goto duped; //prevents checking the other macs when you already see a duplicate
      }
    }
duped:
    //  Serial.println("checking if a new mac is present" ) ; // start new line of output
    if (millis() - lastTime3 > 0) {
      if (dupe == 0) {
        for ( int z = 0; z < 6; ++z ) {
          MacList[MacNum][z] = mac_addr[z], HEX;
          //        Serial.println();
          //        Serial.println();
          //        Serial.println(MacList[MacNum][z]), HEX;
          //        Serial.println(mac_addr[z]), HEX;
        }
        Serial.print("New Mac Detected, total: ");
        MacNum++;
        Serial.println(MacNum);
        Serial.println("_____________________________________________");

      }
      lastTime3 = millis();
    }



  } else {   //if receiving sensor data
    Data_send_status = 1;
    memcpy(&myData, incomingThing, sizeof(myData));

    if (millis() - printTime > 100) { //stores and prints the data every 0.1 second
      //      Serial.println();
      //      Serial.print("big bytes: ");
      //      Serial.println(len);
      // Update the structures with the new incoming data
      sensorData[myData.id - 1].id = myData.id;
      sensorData[myData.id - 1].windSpeed = myData.windSpeed;
      sensorData[myData.id - 1].windDir = myData.windDir;
      sensorData[myData.id - 1].tempC = myData.tempC;
      sensorData[myData.id - 1].capRead = myData.capRead;
      sensorData[myData.id - 1].t = myData.t;
      sensorData[myData.id - 1].h = myData.h;

      printData();
      printTime = millis();
    }
  }
}






void printData() { //prints all data
  Serial.print("Data received from ID: ");
  Serial.print("Data Id: ");
  Serial.println(myData.id);
  Serial.println("WindSpeed(KPH) \tDirection(degrees) \tHeading");
  Serial.print(myData.windSpeed);
  Serial.print("\t\t");
  Serial.print(myData.windDir);
  Serial.print("\t\t\t");
  getHeading(myData.windDir);

  Serial.print("Soil Temp: "); Serial.print(myData.tempC); Serial.print("*C");
  Serial.print("\t\tCapacitive Soil Moisture: "); Serial.println(myData.capRead);
  Serial.print("Ambient Temp: "); Serial.print(myData.t); Serial.print("*C");
  Serial.print("\t\tAmbient Humidity: "); Serial.println(myData.h);
}





void setup() {
  // Initialize Serial Monitor
  Serial.begin(9600);
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP - NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_set_self_role(ESP_NOW_ROLE_COMBO);
  esp_now_register_send_cb(OnDataSent);
  esp_now_register_recv_cb(OnDataRecv);

  //BELOW ONLY FOR CH SLEEPING
  //set firstCluster from memory
  //  getFirstCluster();
  //  //function runs 1 time setup where it pairs to the sensor nodes
  //  if (alwaysCluster == 1) {
  //    Serial.println("Cluster every period");
  //    Cluster = 1;
  //    firstCluster = 1; //forces the next cluster
  //    storeFirstCluster(); //stores next firstCluster
  //  } else if (firstCluster == 0) {
  //    Serial.println("Cluster first period only");
  //    //broadcasts to any node, creates a list of mac addressses
  //    Cluster = 1;
  //    firstCluster = 0;
  //    //sets the new firstCluster to memory
  //    storeFirstCluster();
  //  }


  //delayed:
  //  while (millis()  < 5000) {
  //    //      while (1) delay(1);
  //    //      goto delayed;
  //    //      Serial.println("delaying");
  //    lastTime = 0;
  //  }
  Serial.println("end setup"); //happens immediately then stops
}


//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------


void loop() {
  periodTimer = (millis() - periodStart); //calculates the time since schedule was received

  if ((millis() - ahsecond) > 900) { //how often the broadcast is send out
    ahsecond = millis();
    if ((millis() - lastTime2 < 5000) && Cluster) {
      cluster();
    } else {
      Cluster = false;
      lastTime2 = millis();
    }
  }

  if ((millis() - lastTime > 5000) && scheduling ) {
    //    Serial.println(x);
    if (((millis() - lastTime) - hundred) > 900) { //how often the broadcast is send out
      schedule();
      x++;
      hundred = millis();
      if (x > 3) {
        scheduling = false;
        lastTime = millis();
        x = 0;
      }
    }
    periodStart = millis(); //sets new period start timer
  }



  if ((millis() - asecond) > 1000) { //how often the broadcast is send out
    Serial.print("loop started, period timer: ");
    Serial.println(periodTimer / 1000);
    asecond = millis();

    if ((periodTimer) > (T * 1000)) { //when the period ends
      //      Cluster = true;
      scheduling = true;
      periodStart = millis(); //sets new period start timer
      Serial.println("New Period Start == == == == == == == == == == == == == == == == == == == == == == == == == ");
      Serial.print("Current period: ");
      Serial.println(T);
    }
  }
}


//MUST START THE RESET THE TIMER AFTER THE CLUSTERING PHASE AND ON THE SN


void cluster() {
  Serial.println();
  Serial.println("Always Clustering");
  //broadcast to any nodes every second
  esp_now_send(broadcastAddressALL, (uint8_t *) &Test, sizeof(Test));
}

void schedule() {
  Serial.println("Sending schedule ++++++++++++++++++++++++++++++++++++++++++++++");

  for (int i = 0; i < MacNum; i++) { //synchonizes the system with wiggle room
    uint32_t schedule[] = {i + 1, T};

    //    for (int x = 0; x < 3; ) {
    //      if (((millis() - hundred) > 100)) { //how often the broadcast is send out
    //        x++;
    //        hundred = millis();
    esp_now_send(MacList[i], (uint8_t *) &schedule, sizeof(schedule));
  }
}








//sends the CH to sleep for the rest of the period
void sleep() {
  Serial.println();
  Serial.print("Current Period: ");
  Serial.println(T);
  Serial.println();
  Serial.print("I'm awake, but I'm going into deep sleep mode for seconds : ");
  //  Serial.println((T * 1000) - millis());
  Serial.println(((T * 1000) - millis()) / 1000);
  ESP.deepSleep(((T * 1000) - millis()) * 1000); //sleep for T - Y. Sleep argument is in nanoseconds
}




//reads old firstCluster
void getFirstCluster() {
  int firstClusterOld;
  //Init EEPROM
  EEPROM.begin(4096);

  int address = 0;
  EEPROM.get(address, firstClusterOld);
  Serial.print("Reading firstClusterOld : ");
  Serial.println(firstClusterOld);
  EEPROM.end();
  firstCluster = firstClusterOld;
}

void storeFirstCluster() {
  //Init EEPROM
  EEPROM.begin(4096);

  Serial.print("Storing new firstCluster : ");
  Serial.println(firstCluster);
  //Write data into eeprom
  int address = 0;
  EEPROM.put(address, firstCluster);
  EEPROM.commit();
  EEPROM.end();
}


//stores data that wasn't sent successfully. Stores all 3 nodes data.
void storeData() {
  //Init EEPROM
  EEPROM.begin(4096);
  //Write data into eeprom
  int address = 2048;
  EEPROM.put(address, sensorData);
  EEPROM.commit();

  //Read data from eeprom
  address = 0;
  int readId;
  EEPROM.get(address, readId);
  //Serial.print("Read Id = ");
  //Serial.println(readId);
  address += sizeof(readId); //update address value
  EEPROM.get(address, sensorData); //readParam=EEPROM.readFloat(address);
  //printData(myData2);
  EEPROM.end();
}
