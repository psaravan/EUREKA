/*
  Rui Santos
  Complete project details at https://randomnerdtutorials.com/esp-now-one-to-many-esp8266-nodemcu/

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/

#include <ESP8266WiFi.h>
#include <SPI.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include<ESP8266WiFi.h>
#include <espnow.h>




// Structure example to receive data
// Must match the sender structure
typedef struct Sensor_data {
    double windSpeed;
    int windDir;
    float tempC;
    uint16_t capead;
    float t;
    float h;
} my_Sensor_data;



// Create a struct_message called myData
my_Sensor_data myData;

void OnDataRecv(uint8_t * mac, uint8_t *incomingData, uint8_t len) {
  memcpy(&myData, incomingData, sizeof(myData));
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("Wind Speed: ");
  Serial.println(myData.windSpeed);
  Serial.print("Wind Direction: ");
  Serial.println(myData.windDir);
  Serial.print("Soil Temperature: ");
  Serial.println(myData.tempC);
  Serial.print("Soil Moisture: ");
  Serial.println(myData.capead);
  Serial.print("Temperature: ");
  Serial.println(myData.t);
  Serial.print("Humidity: ");
  Serial.println(myData.h);
  


}

 
void setup() {
  // Initialize Serial Monitor
  Serial.begin(115200);

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_set_self_role(ESP_NOW_ROLE_SLAVE);
    esp_now_register_recv_cb(OnDataRecv);


}
void loop() {}
