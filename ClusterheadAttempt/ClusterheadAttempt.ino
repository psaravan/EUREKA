// 5/30/22 poster demo
#include <ESP8266WiFi.h>
#include <SPI.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include<ESP8266WiFi.h>
#include <espnow.h>




// Structure example to receive data
// Must match the sender structure
typedef struct Sensor_data {
  int id;
  double windSpeed;
  int windDir;
  float tempC;
  uint16_t capRead;
  float t;
  float h;
} my_Sensor_data;


// Create a struct_message called myData
my_Sensor_data myData;

// Create a struct_message called Data1
my_Sensor_data Data1; // Sensor node 1 id is 1
my_Sensor_data Data2; // Sensor node 2 id is 2
my_Sensor_data Data3; // Sensor node 3 id is 3
my_Sensor_data DataAvg1; // Struct for averaging data
my_Sensor_data DataAvg2; // Struct for averaging data

// Create an array with all the structures
my_Sensor_data sensorData[2] = {Data1, Data2};

int myperiod = 25; //period to send to node
unsigned long lastTime = 0;
unsigned long lastTime2 = 0;
unsigned long printTime = 0;
int Global_send_status = 1;
int countAlternate = 0;
bool newPeriod = true;
int x = 0;



//NodeMCUs B,D,F are BROKEN
uint8_t broadcastAddressA[] = {0x4C, 0x75, 0x25, 0x35, 0x85, 0x80}; //NodeMCU A id is 1
uint8_t broadcastAddressC[] = {0x4C, 0x75, 0x25, 0x35, 0xB0, 0x11}; //NodeMCU C id is 3
uint8_t broadcastAddressE[] = {0x4C, 0x75, 0x25, 0x36, 0x57, 0x74}; //NodeMCU E id is 5
uint8_t broadcastAddressG[] = {0x58, 0xBF, 0x25, 0xDA, 0x84, 0x08}; //NodeMCU G id is 7
uint8_t broadcastAddressH[] = {0x40, 0x91, 0x51, 0x52, 0xD4, 0xB3}; //NodeMCU H id is 8
uint8_t broadcastAddressI[] = {0x58, 0xBF, 0x25, 0xDA, 0xA9, 0x19}; //NodeMCU I id is 9


void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {
  char macStr[18];
  //  Serial.print("Packet to:");
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  //  Serial.print(macStr);
  //  Serial.print(" send status: ");
  Global_send_status = sendStatus;
  if (sendStatus == 0) {
    //    Serial.println("Period Delivery success");
    newPeriod = false;
  }
  else {
    //    Serial.println("Period Delivery fail");
  }
}

void OnDataRecv(uint8_t * mac_addr, uint8_t *incomingData, uint8_t len) {
  char macStr[18];
  //  Serial.print("Packet received from: ");
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  //  Serial.println(macStr);
  memcpy(&myData, incomingData, sizeof(myData));
  if (millis() - printTime > 1000) {
    // Update the structures with the new incoming data
    sensorData[myData.id - 1].id = myData.id;
    sensorData[myData.id - 1].windSpeed = myData.windSpeed;
    sensorData[myData.id - 1].windDir = myData.windDir;
    sensorData[myData.id - 1].tempC = myData.tempC;
    sensorData[myData.id - 1].capRead = myData.capRead;
    sensorData[myData.id - 1].t = myData.t;
    sensorData[myData.id - 1].h = myData.h;

    Serial.println("\n");
    Serial.print("Bytes received: ");
    Serial.println(len);
    Serial.print("Data Id: ");
    Serial.println(sensorData[myData.id - 1].id);
    /*
      Serial.print("Wind Speed: ");
      Serial.println(sensorData[myData.id - 1].windSpeed);
      Serial.print("Wind Direction: ");
      Serial.println(sensorData[myData.id - 1].windDir);
    */
    Serial.println("WindSpeed(KPH) \tDirection(degrees) \tHeading");
    Serial.print(sensorData[myData.id - 1].windSpeed);
    Serial.print("\t\t");
    Serial.print(sensorData[myData.id - 1].windDir);
    Serial.print("\t\t\t");
    getHeading(sensorData[myData.id - 1].windDir);

    Serial.print("Soil Temp: "); Serial.print(sensorData[myData.id - 1].tempC); Serial.print("*C");
    Serial.print("\t\tCapacitive Soil Moisture: "); Serial.println(sensorData[myData.id - 1].capRead);
    Serial.print("Ambient Temp: "); Serial.print(sensorData[myData.id - 1].t); Serial.print("*C");
    Serial.print("\t\tAmbient Humidity: "); Serial.println(sensorData[myData.id - 1].h);

    /*
        Serial.print("Soil Temperature: ");
        Serial.println(sensorData[myData.id - 1].tempC);
        Serial.print("Soil Moisture: ");
        Serial.println(sensorData[myData.id - 1].capRead);
        Serial.print("Temperature: ");
        Serial.println(sensorData[myData.id - 1].t);
        Serial.print("Humidity: ");
        Serial.println(sensorData[myData.id - 1].h);
    */
    Serial.println();
    x++;
    Serial.print("minutes since last average is : ");
    Serial.println(x);
    Serial.println("\n");
    printTime = millis();

    if (myData.id == 1) {
      // Add to the structures with the new incoming data
      DataAvg1.windSpeed += myData.windSpeed;
      DataAvg1.tempC += myData.tempC;
      DataAvg1.capRead += myData.capRead;
      DataAvg1.t += myData.t;
      DataAvg1.h += myData.h;
    }
  }
}


void setup() {
  // Initialize Serial Monitor
  Serial.begin(9600);
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_set_self_role(ESP_NOW_ROLE_COMBO);
  esp_now_register_send_cb(OnDataSent);
  esp_now_add_peer(broadcastAddressE, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
  esp_now_add_peer(broadcastAddressG, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
  esp_now_register_recv_cb(OnDataRecv);
}

void loop() {
  if (x == 5) { //take the average over 5 minutes
    Serial.println("\n");
    Serial.print("5 Minute AVERAGES: " );
    Serial.print("\nWindSpeed: "); Serial.print(DataAvg1.windSpeed / 5); Serial.print(" kph");
    Serial.print("\nSoil Temp: "); Serial.print(DataAvg1.tempC / 5); Serial.print("*C");
    Serial.print("\t\tCapacitive Soil Moisture: "); Serial.println(DataAvg1.capRead / 5);
    Serial.print("Ambient Temp: "); Serial.print(DataAvg1.t / 5); Serial.print("*C");
    Serial.print("\t\tAmbient Humidity: "); Serial.println(DataAvg1.h / 5); Serial.print("%");
    x = 0;
    DataAvg1.windSpeed = 0;
    DataAvg1.tempC = 0;
    DataAvg1.capRead = 0;
    DataAvg1.t = 0;
    DataAvg1.h = 0;
  }

  // After 10 second intervals change the period, if recieved successfully
  //  if (((millis() - lastTime) > 40000) && Global_send_status == 0 && newPeriod == 0) {
  //    newPeriod = true;
  //
  //    countAlternate = countAlternate + 1;
  //    if (countAlternate % 2 == 0) {
  //      myperiod = 20;
  //    }
  //    else {
  //      myperiod = 10;
  //    }
  //    Serial.print("Current Period:");
  //    Serial.println(myperiod);
  //    Serial.println();
  //    //    Serial.println(countAlternate);
  //    //    Serial.println();
  //    lastTime = millis();
  //  }

  //  if ((millis() - lastTime2 > 1000)) { //print period status every second
  //    Serial.println(newPeriod);
  //    lastTime2 = millis();
  //  }

  // When the period is not recieved correctly yet, keep sending the period
  //if (Global_send_status == 1 || newPeriod) {
  //  if (Global_send_status == 1 || newPeriod) {
  //    delay(100);
  //    Serial.println("sending new period: ");
  //    Serial.println(myperiod);
  //    esp_now_send(broadcastAddressE, (uint8_t *) &myperiod, sizeof(myperiod));    //for sending myperiod to node.  Not sure if the formatting is correct)
  //    esp_now_send(broadcastAddressG, (uint8_t *) &myperiod, sizeof(myperiod));    //for sending myperiod to node.  Not sure if the formatting is correct)
  //  }
  //  else{
  //    if ((millis() >= 120000)) { //awake untill data recieved
  //          Serial.println("I'm awake, but I'm going into deep sleep mode for seconds:");
  //          Serial.println(60);
  //          Serial.println(((60 * 1000) - millis()));
  //          ESP.deepSleep(((60 * 1000) - millis()) * 1000); //sleep for T - Y
  //     }
  //  }
}

void getHeading(int direction) {
  if (direction < 22)
    Serial.println("N");
  else if (direction < 67)
    Serial.println("NE");
  else if (direction < 112)
    Serial.println("E");
  else if (direction < 157)
    Serial.println("SE");
  else if (direction < 212)
    Serial.println("S");
  else if (direction < 247)
    Serial.println("SW");
  else if (direction < 292)
    Serial.println("W");
  else if (direction < 337)
    Serial.println("NW");
  else
    Serial.println("N");
}
